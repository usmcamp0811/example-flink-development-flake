{
  description = "An example environment that you can use to Develop and Test Flink Jobs.";
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    flake-parts.url = "github:hercules-ci/flake-parts";
    systems.url = "github:nix-systems/default";
    process-compose-flake.url = "github:Platonic-Systems/process-compose-flake";
    services-flake.url = "github:juspay/services-flake";
    campground.url = "gitlab:usmcamp0811/dotfiles";
    northwind.url = "github:pthom/northwind_psql";
    northwind.flake = false;
    poetry2nix = {
      url = "github:nix-community/poetry2nix";
    };
  };
  outputs = inputs:
    inputs.flake-parts.lib.mkFlake { inherit inputs; } {
      systems = import inputs.systems;
      imports = [
        inputs.process-compose-flake.flakeModule
      ];
      perSystem = { self', pkgs, lib, system, ... }: {
        _module.args.pkgs = import inputs.nixpkgs {
          inherit system;
          overlays = [
              (final: prev: {
                campground = inputs.campground.packages.${prev.system};
                example-flink-job = pkgs.callPackage ./example-flink-job/default.nix { };
                example-beam-job = pkgs.callPackage ./example-beam-job/default.nix { };
              })
              inputs.poetry2nix.overlays.default
          ];
          config = { };
        };
        process-compose."default" = { config, ... }:
          let
            #TODO: Move ports here for easy changing
            dbName = "sample";
          in
          {
            imports = [
              inputs.services-flake.processComposeModules.default
              ./services/kafka.nix
              ./services/flink.nix
            ];
          };

        devShells.default = pkgs.mkShell {
          nativeBuildInputs = [ pkgs.just pkgs.example-flink-job pkgs.example-beam-job ];
        };
      };
    };
}
