import logging
import os
import sys  # Added missing import

import apache_beam as beam
from apache_beam.options.pipeline_options import PipelineOptions, FlinkPipelineOptions

class ReverseTextFn(beam.DoFn):
    def process(self, element):
        """
        Simple DoFn that reverses any text that its given.
        """
        logging.info(f"Reversing Text: {element}")
        yield element[::-1]

def run_example_beam_job(broker):
    options = PipelineOptions()
    flink_options = options.view_as(FlinkPipelineOptions)
    flink_options.runner = 'FlinkRunner'
    flink_options.flink_master = 'localhost:8081'  # Update with your Flink Job Manager address
    flink_options.streaming = True

    with beam.Pipeline(options=options) as p:
        kafka_read_config = {
            'bootstrap.servers': broker,  # Correct key for bootstrap servers
            'topic': 'example-input-topic'  # Correct key for topic
        }
        kafka_write_config = {
            'bootstrap.servers': broker,  # Correct key for bootstrap servers
            'topic': 'example-output-topic'  # Correct key for topic
        }

        (p
         | 'Read from Kafka' >> beam.io.kafka.ReadFromKafka(
                consumer_config=kafka_read_config,
                value_decoder=beam.coders.BytesCoder()
            )
         | 'Reverse Text' >> beam.ParDo(ReverseTextFn())
         | 'Encode String' >> beam.Map(lambda x: x.encode('utf-8'))
         | 'Write to Kafka' >> beam.io.kafka.WriteToKafka(
                producer_config=kafka_write_config,
                value_encoder=beam.coders.BytesCoder()
            ))

if __name__ == '__main__':
    broker = os.getenv("KAFKA_BROKER", "localhost:9092")
    logging.basicConfig(stream=sys.stdout, level=logging.INFO, format="%(message)s")
    run_example_beam_job(broker)
