# Example Flink Development Environment

Welcome to the Example Flink Development Environment! This project is my initial foray into utilizing the [services-flake](https://github.com/juspay/services-flake). The goal is to establish a comprehensive setup for Flink development, including a local Kafka cluster, schema registry, Kafka Connect, and Apache Kafka HQ for monitoring.

## Getting Started

To run the development environment, use the following command:

```sh
nix run gitlab:usmcamp0811/example-flink-development-flake
```

This setup aims to streamline your Flink development process, providing all necessary tools and services out of the box.

![screenshot](./image.png)
