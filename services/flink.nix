{ config, pkgs, services, lib, ... }:
let
  # example-flink-job = import ./example-flink-job/default.nix {
  #   inherit pkgs lib;
  # };
in
{
  settings.processes.jobmanager = {
    command = pkgs.writeShellApplication {
      runtimeInputs = [ pkgs.bash pkgs.example-flink-job ];
      text = ''
        export KAFKA_BROKER="localhost:9094"
        export PATH=${pkgs.example-flink-job.python}/bin/:$PATH
        export PYTHONPATH="${pkgs.example-flink-job.python}/lib/python3.11/site-packages"
        export PYFLINK_PYTHON="${pkgs.example-flink-job.python}/bin/python"
        export JAVA_HOME=${pkgs.openjdk11};
        export FLINK_HOME=${pkgs.flink}/opt/flink
        export FLINK_CONF_DIR="${pkgs.example-flink-job.conf}/conf";
        ${pkgs.flink}/opt/flink/bin/jobmanager.sh start-foreground
      '';
      name = "jobmanager";
    };
    depends_on."k1".condition = "process_healthy";
    availability.restart = "on_failure";
    readiness_probe = {
      exec.command = "${pkgs.curl}/bin/curl http://localhost:8081/jobmanager/metrics";
      initial_delay_seconds = 10;
      period_seconds = 10;
      timeout_seconds = 4;
      success_threshold = 3;
      failure_threshold = 5;
    };
  };

  settings.processes.taskmanager = {
    command = pkgs.writeShellApplication {
      runtimeInputs = [ pkgs.bash pkgs.example-flink-job ];
      text = ''
        export KAFKA_BROKER="localhost:9094"
        export PATH=${pkgs.example-flink-job.python}/bin/:$PATH
        export PYTHONPATH="${pkgs.example-flink-job.python}/lib/python3.11/site-packages"
        export PYFLINK_PYTHON="${pkgs.example-flink-job.python}/bin/python"
        export JAVA_HOME=${pkgs.openjdk11};
        export FLINK_HOME=${pkgs.flink}/opt/flink
        export FLINK_CONF_DIR="${pkgs.example-flink-job.conf}/conf";
        ${pkgs.flink}/opt/flink/bin/taskmanager.sh start-foreground
      '';
      name = "taskmanager";
    };
    depends_on."k1".condition = "process_healthy";
    availability.restart = "on_failure";
    readiness_probe = {
      exec.command = "${pkgs.curl}/bin/curl http://localhost:8081/jobmanager/metrics";
      initial_delay_seconds = 10;
      period_seconds = 10;
      timeout_seconds = 4;
      success_threshold = 3;
      failure_threshold = 5;
    };
  };

  # settings.processes.flink-job = {
  #   command = pkgs.writeShellApplication {
  #     runtimeInputs = [ pkgs.bash pkgs.example-flink-job ];
  #     text = ''
  #       export KAFKA_BROKER="localhost:9094"
  #       ${pkgs.example-flink-job}/bin/example-flink-job
  #     '';
  #     name = "flink-job";
  #   };
  #   depends_on."jobmanager".condition = "process_healthy";
  #   depends_on."taskmanager".condition = "process_healthy";
  # };
}
