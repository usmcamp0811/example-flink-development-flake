{ config, pkgs, services, lib, ... }:
let
# TODO: Set all ports from variables so we are DRY but more importantly we can change them easily

karapace-json = pkgs.writeTextFile {
  name = "config.json";
  text = lib.generators.toJSON {} {
      bootstrap_uri = "127.0.0.1:9094";
      host = "127.0.0.1";
      port = 18436;
      registry_user = false;
      karapace_registry = true;
  };
};

akhq-settings-yaml = pkgs.writeTextFile {
  name = "akhq-settings.yaml";
  text = lib.generators.toYAML {} {
    akhq = {
      connections = {
        campground = {
          # TODO: Add Kafka Connect
          # connect = [
          #   {
          #     name = "dev-cluster";
          #     url = "http://localhost:8323";
          #   }
          # ];
          properties = {
            "bootstrap.servers" = "localhost:9094";
          };
          schema-registry = {
            url = "http://localhost:18436";
          };
        };
      };
    };
    micronaut = {
      server = {
        port = 18421;
        host = "localhost";
      };
    };
  };
};
in
{
  services.zookeeper."z1".enable = true;
  services.zookeeper."z1".port = 2182;

  services.apache-kafka."k1" = {
    enable = true;
    port = 9094;
    settings = {
      "offsets.topic.replication.factor" = 1;
      "zookeeper.connect" = [ "localhost:2182" ];
    };
  };

  settings.processes.k1.depends_on."z1".condition = "process_healthy";

  settings.processes.akhq = {
    command = pkgs.writeShellApplication {
      runtimeInputs = [ pkgs.bash pkgs.campground.akhq ];
      text = ''
        export MICRONAUT_CONFIG_FILES="${akhq-settings-yaml}"
        ${pkgs.campground.akhq}/bin/akhq
      '';
      name = "akhq";
    };
    depends_on."k1".condition = "process_healthy";
    availability.restart = "on_failure";
  };

  settings.processes.schema-registry = {
    command = pkgs.writeShellApplication {
      runtimeInputs = [ pkgs.bash pkgs.campground.karapace ];
      text = ''
        ${pkgs.campground.karapace}/bin/karapace ${karapace-json}
      '';
      name = "schema-registry";
    };
    depends_on."k1".condition = "process_healthy";
    availability.restart = "on_failure";
  };

  settings.processes.test-message = {
    command = pkgs.writeShellApplication {
      runtimeInputs = [ pkgs.bash config.services.apache-kafka.k1.package ];
      text = ''
        sleep 5
        echo 'THIS IS A WORK IN PROGRESS' | kafka-console-producer.sh --broker-list localhost:9094 --topic example-input-topic
        sleep 2
        echo 'I HOPE THIS HELPS' | kafka-console-producer.sh --broker-list localhost:9094 --topic example-input-topic
      '';
      name = "test-message";
    };
    depends_on."k1".condition = "process_healthy";
  };

}
